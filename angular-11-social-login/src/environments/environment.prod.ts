export const environment = {
  production: true,
  apiBaseUrl: 'http://nacer-server.ghazelatc.com/',
  clientUrl: '?redirect_uri=http://nacer-client.ghazelatc.com/login'
};
